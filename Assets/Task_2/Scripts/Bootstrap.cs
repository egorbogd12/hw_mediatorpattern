using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private GameplayMediator _mediator;

    private void Awake()
    {
       // _mediator.Initialize();
        _player.Initialize();
    }
}
