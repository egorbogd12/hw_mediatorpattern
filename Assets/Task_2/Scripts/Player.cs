using System;
using UnityEngine;
using Zenject;

public class Player : MonoBehaviour
{ 
    private GameplayMediator _mediator;

    public event Action<int> OnHPChanged;

    private int _startLevel = 1;
    private int _startHP = 10;

    private int _lvlToAdd = 1;
    private int _hPToAdd = 3;

    private int _currentLevel;
    private int _currentHP;

    [Inject]
    private void Construct(GameplayMediator mediator)
    {
        _mediator = mediator;
        
        Initialize();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            UpgradeLVL(_lvlToAdd);
            _mediator.UpdateLVLVisual(_currentLevel);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            AddHP(_hPToAdd);
            OnHPChanged?.Invoke(_currentHP);
        }
    }

    public void Initialize()
    {
        _currentLevel = _startLevel;
        _currentHP = _startHP;

        _mediator.UpdateLVLVisual(_currentLevel);
        OnHPChanged?.Invoke(_currentHP);
    }

    private void UpgradeLVL(int valueToAdd)
    {
        _currentLevel += valueToAdd;
    }

    private void AddHP(int valueToAdd)
    {
        _currentHP += valueToAdd;
    }
}
