﻿using UnityEngine;
using TMPro;

public abstract class BaseUI : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI _valueText;

    public virtual void UpdateVisual(int currentValue)
    {
        _valueText.text = currentValue.ToString();
    }
}
