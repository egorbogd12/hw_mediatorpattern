using UnityEngine;
using Zenject;

public class MediatorInstaller : MonoInstaller
{
    [SerializeField] private Player _player;
    [SerializeField] private LVLUI _lvlui;
    [SerializeField] private HPUI _hpui;
    
    public override void InstallBindings()
    {
        BindMediator();
        // BindUI();
        // BindPlayer();
    }

    private void BindUI()
    {
        Container.Bind<LVLUI>().FromInstance(_lvlui).AsSingle();
        Container.Bind<HPUI>().FromInstance(_hpui).AsSingle();
    }

    private void BindPlayer()
    {
        Container.Bind<Player>().FromInstance(_player).AsSingle();
    }

    private void BindMediator()
    {
        GameplayMediator  mediator = new GameplayMediator(_player,_lvlui, _hpui);
       
       Container.BindInterfacesAndSelfTo<GameplayMediator>().FromInstance(mediator).AsSingle();
    }
}
