﻿using System;

public class GameplayMediator : IDisposable
{
    private HPUI _hpUI;
    private LVLUI _lVLUI;
    private Player _player;
    
    public GameplayMediator (Player player, LVLUI lvlui, HPUI hpui)
    {
        _player = player;
        _lVLUI = lvlui;
        _hpUI = hpui;
        
        _player.OnHPChanged += player_OnHPChanged;
    }

    private void player_OnHPChanged(int newValue)
    {
        _hpUI.UpdateVisual(newValue);
    }
    
    public void UpdateLVLVisual(int newValue)
    {
        _lVLUI.UpdateVisual(newValue);
    }

    public void Dispose()
    {
        _player.OnHPChanged -= player_OnHPChanged;
    }
}